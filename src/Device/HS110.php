<?php declare(strict_types=1);

namespace Weks\TPLink\Device;

use Weks\TPLink\Device;
use Weks\TPLink\Device\Feature\EmeterTrait;
use Weks\TPLink\Device\Feature\RelayTrait;

class HS110 extends Device
{
    use EmeterTrait;
    use RelayTrait;
}