<?php declare(strict_types=1);

namespace Weks\TPLink\Device\Feature;

trait RelayTrait
{
    public function turnOn()
    {
        return $this->setState(true);
    }

    public function turnOff()
    {
        return $this->setState(false);
    }

    public function setState(bool $state)
    {
        $result = $this->_client->passThru(
            $this->appServerUrl,
            $this->deviceId,
            [
                'system' => [
                    'set_relay_state' => [
                        'state' => $state ? 1 : 0,
                    ],
                ]
            ]
        );

        return !$result['system']['set_relay_state'];
    }

    public function getState(): bool
    {
        return !!$this->relay_state;
    }
}