<?php declare(strict_types=1);

namespace Weks\TPLink\Device\Feature;

trait EmeterTrait
{
    public function getPowerUsage()
    {
        $result = $this->_client->passThru(
            $this->appServerUrl,
            $this->deviceId,
            [
                'emeter'   => [
                    'get_realtime'  => null,
                ]
            ]
        );

        $this->_data['power'] = array(
            'current'   => $result['emeter']['get_realtime']['current'],
            'voltage'   => $result['emeter']['get_realtime']['voltage'],
            'power'     => $result['emeter']['get_realtime']['power'],
            'total'     => $result['emeter']['get_realtime']['total'],
        );

        return $this->_data['power'];
    }
}