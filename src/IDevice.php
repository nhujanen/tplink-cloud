<?php declare(strict_types=1);

namespace Weks\TPLink;

interface IDevice
{
    const   TYPE_SMARTPLUG          = 'IOT.SMARTPLUGSWITCH';

    const   FEATURE_SWITCH          = 'SWI';
    const   FEATURE_TIMER           = 'TIM';
    const   FEATURE_ENERGYMONITOR   = 'ENE';
    
}