<?php declare(strict_types=1);

namespace Weks\TPLink;

use \Ramsey\Uuid\Uuid;
use Weks\TPLink\Client\ClientAbstract;
use Weks\TPLink\Client\ConnectionException;

class Client extends ClientAbstract
{
    protected   $_uuid;
    protected   $_token;

    public function __construct(string $token, string $uuid = null)
    {
        $this->_token   = $token;
        $this->_uuid    = is_null($uuid) ? (string) Uuid::uuid4() : $uuid;
    }

    public static function login(string $username, string $password, string $uuid = null)
    {
        $_username  = $username;
        $_password  = $password;
        $_uuid      = is_null($uuid) ? (string) Uuid::uuid4() : $uuid;

        $result = static::httpPost(
            'https://wap.tplinkcloud.com',
            '/', 
            [
                'method'        => 'login',
                'params'        => [
                    'appType'       => 'Kasa_Android',
                    'appName'       => 'Kasa_Android',
                    'termID'        => $_uuid,
                    'appVer'        => '1.4.4.607',
                    'ospf'          => 'Android+6.0.1',
                    'netType'       => 'wifi',
                    'locale'        => 'es_ES',
                    'cloudPassword' => $_password,
                    'cloudUserName' => $_username,
                    'terminalUUID'  => $_uuid,
                ],
            ]
        );

        if ($result['error_code'] <> 0)
            throw new ConnectionException($result['msg'], $result['error_code']);

        return new static($result['result']['token'], $_uuid);
    }

    public function getDevices(array $types = [])
    {
        $_typeFilter    = count($types) ? $types : false;

        $result = static::httpPost(
            'https://wap.tplinkcloud.com', 
            '',
            [
                'method'        => 'getDeviceList',
                'params'        => [
                    'appName'       => 'Kasa_Android',
                    'termID'        => $this->_uuid,
                    'appVer'        => '1.4.4.607',
                    'ospf'          => 'Android+6.0.1',
                    'netType'       => 'wifi',
                    'locale'        => 'es_ES',
                    'token'         => $this->_token,
                ],
            ]
        );

        if ($result['error_code'] <> 0)
            throw new ConnectionException($result['msg'], $result['error_code']);        

        return array_filter(array_map(
            function($device) use ($_typeFilter) {
                if ($_typeFilter && !in_array( $device['deviceType'], $_typeFilter ))
                    return null;

                return Device::factoryFromModel( $this, $device['deviceModel'], $device );
            },
            $result['result']['deviceList']
        ));
    }
    
    public function passThru(string $url, string $deviceId, array $request)
    {
        $result = static::httpPost(
            $url, 
            '/',
            [
                'method'        => 'passthrough',
                'params'        => [
                    'appName'       => 'Kasa_Android',
                    'termID'        => $this->_uuid,
                    'appVer'        => '1.4.4.607',
                    'ospf'          => 'Android+6.0.1',
                    'netType'       => 'wifi',
                    'locale'        => 'es_ES',
                    'token'         => $this->_token,

                    'deviceId'      => $deviceId,
                    'requestData'   => json_encode($request),
                ],
            ]
        );

        if ($result['error_code'] <> 0)
            throw new ConnectionException($result['msg'], $result['error_code']);        


        return json_decode($result['result']['responseData'], true);
    }
    

}