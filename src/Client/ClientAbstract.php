<?php declare(strict_types=1);

namespace Weks\TPLink\Client;

use GuzzleHttp\Client AS HttpClient;

abstract class ClientAbstract
{
    protected static function httpPost(string $base, string $uri, array $payload): array
    {
        $client = new HttpClient([
            'base_uri'      => $base,
            'timeout'       => 2.0,
            'headers'       => [
                'cache-control' => 'no-cache',
                'User-Agent'    => 'Dalvik/2.1.0 (Linux; U; Android 6.0.1; A0001 Build/M4B30X)',
            ],
            'json'          => $payload,
        ]);

        $request = $client->post($uri);

        return json_decode( (string) $request->getBody()->getContents(), true );
    }

}