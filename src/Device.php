<?php declare(strict_types=1);

namespace Weks\TPLink;

use Weks\TPLink\Device\Generic;
use Weks\TPLink\Device\HS110;

abstract class Device implements IDevice
{
    static  $deviceMap  = [
        'HS110'     => HS110::class,
        'GENERIC'   => Generic::class,
    ];

    protected   $_online;
    protected   $_client;
    protected   $_data  = [];

    public static function factoryFromModel(Client &$client, string $model, array $data): self
    {
        $_model         = strtoupper(substr($model, 0, 5));
        $deviceClass    = array_key_exists( $_model, static::$deviceMap ) ?
            static::$deviceMap[ $_model ] : static::$deviceMap['GENERIC'];

        return new $deviceClass($client, $data);
    }

    protected function __construct(Client &$client, array $data)
    {
        $this->_client  = $client;
        $this->_data    = $data;
        $this->_online  = !!$data['status'] ?? false;
    }

    public function __get($name)
    {
        return array_key_exists($name, $this->_data) ?
            $this->_data[$name] : null;
    }

    public function isOnline(): bool
    {
        return $this->_online;
    }

    public function getInfo()
    {
        $result = $this->_client->passThru(
            $this->appServerUrl,
            $this->deviceId,
            [
                'system'    => [
                    'get_sysinfo'   => '',
                ]
            ]
        );
        
        $this->_data['relay_state'] = $result['system']['get_sysinfo']['relay_state'];
        $this->_data['on_time']     = $result['system']['get_sysinfo']['on_time'];
        $this->_data['led_off']     = $result['system']['get_sysinfo']['led_off'];
        $this->_data['latitude']    = $result['system']['get_sysinfo']['latitude'];
        $this->_data['longitude']   = $result['system']['get_sysinfo']['longitude'];

        return $result['system']['get_sysinfo'];
    }
}